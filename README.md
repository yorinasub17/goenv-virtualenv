# goenv-virtualenv

`goenv-virtualenv` is a
[`goenv`](https://github.com/syndbg/goenv#command-reference) plugin that
provides features to manage python like virtualenvs for golang on Unix-like
systems.


## Commands

- `goenv virtualenv <GO_VERSION> <VIRTUALENV_NAME>`
    * This will create a new goenv version with the given virtualenv name using
      the provided golang version. This will be a completely isolated
      environment, with its own GOPATH directory.

- `goenv global/local <VIRTUALENV_NAME>`
    * Enables the go virtualenv. As part of this, all packages will now point
      to the virtualenv's package directory (located at `$(goenv prefix)/thirdparty`).


## TODO

- Robust testing
- More error checking (e.g bad args)
